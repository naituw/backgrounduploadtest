//
//  AppDelegate.h
//  BackgroundUploadTest
//
//  Created by wutian on 2018/11/26.
//  Copyright © 2018 wutian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

