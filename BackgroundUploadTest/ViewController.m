//
//  ViewController.m
//  BackgroundUploadTest
//
//  Created by wutian on 2018/11/26.
//  Copyright © 2018 wutian. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <NSURLConnectionDelegate, NSURLConnectionDataDelegate, NSURLConnectionDownloadDelegate>

@property (nonatomic, strong) NSURLConnection * currentConnection;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self fakeUploadLength:512 * 1024 * 1024];
}

- (void)fakeUploadLength:(NSInteger)length {
    
    void * fakeData = malloc(length);
    NSData * data = [NSData dataWithBytes:fakeData length:length];
    free(fakeData);
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://baidu.com"]];
    request.HTTPMethod = @"POST";
    request.HTTPBody = data;
    
    NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    self.currentConnection = connection;
    
    [connection start];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"Connection Receive Response: %ld", [(NSHTTPURLResponse *)response statusCode]);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection Failed: %@", error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"Connection Finished");
    
    [self fakeUploadLength:512 * 1024];
}

- (void)connection:(NSURLConnection *)connection   didSendBodyData:(NSInteger)bytesWritten
 totalBytesWritten:(NSInteger)totalBytesWritten
totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite
{
    if (!totalBytesExpectedToWrite) {
        return;
    }
    NSLog(@"Connection Progress Update: %f", (double)totalBytesWritten / (double)totalBytesExpectedToWrite);
}

@end
